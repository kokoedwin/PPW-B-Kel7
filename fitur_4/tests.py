from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from fitur_2.models import DataProfile
from fitur_1.models import Status
import unittest

# Create your tests here.
class StatisticUnitTest(TestCase):
    def test_statistic_url_is_exist(self):
        response = Client().get('/statistic/')
        self.assertEqual(response.status_code, 200)

    def test_statistic_using_index_func(self):
        found = resolve('/statistic/')
        self.assertEqual(found.func, index)
