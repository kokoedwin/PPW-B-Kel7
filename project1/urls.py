'''
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Homex
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
'''

from django.conf.urls import url, include
from django.contrib import admin
import fitur_1.urls as fitur_1
import fitur_3.urls as fitur_3
import fitur_2.urls as fitur_2
import fitur_4.urls as dashboard
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^profile/', include(fitur_2, namespace='profile')),
    url(r'^add_friend/', include(fitur_3,namespace='add_friend')),
    url(r'^status/', include(fitur_1, namespace= 'fitur-1')),
    url(r'^statistic/', include(dashboard, namespace= 'dashboard')),
    url(r'^$', RedirectView.as_view(url="/status/", permanent="true"), name='index'),
]
