from django import forms

class Status_form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }

    updatestat_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 3,
        'class': 'todo-form-textarea',
        'placeholder':'Apa yang ada dipikiran anda...'
    }

    updatestat = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=updatestat_attrs))
