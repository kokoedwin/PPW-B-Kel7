from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status
from .models import Status
from .forms import Status_form

class fitur1UnitTest(TestCase):

    def test_fitur_1_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_fitur1_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_todo(self):
        new_activity = Status.objects.create(updatestat='update dulu boss')

        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_form(data={'updatestat': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['updatestat'],
            ["This field is required."]
        )
    def test_fitur1_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/status/add_status', {'updatestat': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_fitur1_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/status/add_status', {'updatestat': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
