from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_form
from .models import Status

# Create your views here.
response = {}
def index(request):    
    response['author'] = "Sukma Firdaus" #TODO Implement yourname
    status = Status.objects.all()
    response['status'] = status
    html = 'fitur_1/fitur_1.html'
    response['Status_form'] = Status_form
    return render(request, html, response)

def add_status(request):
    form = Status_form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['updatestat'] = request.POST['updatestat']
        status = Status(updatestat=response['updatestat'])
        status.save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')
