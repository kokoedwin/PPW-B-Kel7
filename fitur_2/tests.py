from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, name,birthday,gender,email,description,expert

class ProfileUnitTest(TestCase):
    def test_fitur_2_url_is_exist(self):
    	response = Client().get('/profile/')
    	self.assertEqual(response.status_code, 200)

    def test_index_function_is_used(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_all_profile_is_ready(self):
        self.assertIsNotNone(name)
        self.assertIsNotNone(birthday)
        self.assertIsNotNone(gender)
        self.assertIsNotNone(email)

    def test_description_is_written(self):
        self.assertIsNotNone(description)

        self.assertTrue(len(description) >= 20)

    def test_expertise_is_more_than_3_and_less_than_5(self):
        self.assertTrue(len(expert) >= 3 and len(expert) <=5)

    