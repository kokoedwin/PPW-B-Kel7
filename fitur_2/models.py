from django.db import models


class DataProfile(models.Model):
	name = models.CharField(max_length=30)
	birthday = models.CharField(max_length=20)
	gender = models.CharField(max_length=6)
	expertise = models.TextField(max_length=200)
	description = models.CharField(max_length=140)
	email = models.CharField(max_length=30)
