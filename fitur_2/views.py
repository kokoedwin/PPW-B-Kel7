from django.shortcuts import render
from .models import DataProfile


response={}

name = 'Grup 7'
birthday = '04 Oktober 2017'
gender = '-'
email = 'grup7@mail.com'
description = "Terdiri dari Taliya, Edwin, Daus, Albert"
expert = ["push", "pull", "clone"]

profil = DataProfile(name = name, birthday = birthday, gender= gender, email = email, description=description, expertise=expert)


def index(request):
	response = {'name' : profil.name, 'birthday' : profil.birthday, 'gender': profil.gender, 'expertise': profil.expertise, 'description' : profil.description, 'email': profil.email}
	html = 'fitur_2/fitur_2.html'
	return render(request, html, response)