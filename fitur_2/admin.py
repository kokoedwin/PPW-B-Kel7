from django.contrib import admin
from .models import DataProfile

admin.site.register(DataProfile)
