from django.shortcuts import render
from django.http import HttpResponseRedirect,HttpResponse
from .forms import add_friend_Form
from .models import Friends
# Create your views here.
friend_list = {}
response = {}
def index(request):

    #response['author'] = "" #TODO Implement yourname
    friends = Friends.objects.all()
    response['friends'] = friends
    html = 'fitur_3.html'
    response['add_friend_Form'] = add_friend_Form
    return render(request, html, response)


def add_friend(request):
    form = add_friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        friends = Friends(name=response['name'],url=response['url'])
        friends.save()
        return HttpResponseRedirect('/add_friend/')
    else:
        return HttpResponseRedirect('/add_friend/')
