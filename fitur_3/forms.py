from django import forms


class add_friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    nama_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Masukan nama...'
    }
    url_attrs = {
        'type': 'url',
        'class': 'todo-form-textarea',
        'placeholder':'Masukan url...'
    }

    name = forms.CharField(label='Name', required=True, max_length=27, widget=forms.TextInput(attrs=nama_attrs))
    url = forms.URLField(label='URL', required=True, widget=forms.URLInput(attrs=url_attrs))
