from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .forms import add_friend_Form

class Lab3Test(TestCase):
    def test_fitur_3_url_is_exist(self):
        response = Client().get('/add_friend/')
        self.assertEqual(response.status_code,200)

    def test_fitur_3_using_index_func(self):
        found = resolve('/add_friend/')
        self.assertEqual(found.func, index)

    def test_form_validation_for_blank_items(self):
        form = add_friend_Form(data={'name': '', 'url': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['url'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )

    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = add_friend_Form()
        self.assertIn('class="todo-form-input', form.as_p())
        self.assertIn('id="id_name"', form.as_p())
        self.assertIn('class="todo-form-textarea', form.as_p())
        self.assertIn('id="id_url', form.as_p())

    def test_fitur_3_post_success_and_render_the_result(self):
        test = 'Anonymous'
        urlTest='https://www.google.com'
        response_post = Client().post('/add_friend/add_friend', {'name': test, 'url': urlTest})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/add_friend/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_fitur_3_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/add_friend/add_friend', {'name': '', 'url': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/add_friend/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
